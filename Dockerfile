FROM dudash/jenkins-slave-nodejs6

USER root

RUN yum -y install xorg-x11-server-Xvfb; yum clean all

# upstream tries to enable nodejs8 (but has only nodejs6), so had to duplicate and override
COPY contrib/bin/scl_enable /usr/local/bin/scl_enable

USER 1001

RUN source scl_enable rh-nodejs6 && npm install mocha@^5.2.0 nightmare@^2.10.0 -g